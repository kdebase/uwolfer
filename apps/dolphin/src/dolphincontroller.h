/***************************************************************************
 *   Copyright (C) 2006 by Peter Penz (peter.penz@gmx.at)                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA          *
 ***************************************************************************/

#ifndef DOLPHINCONTROLLER_H
#define DOLPHINCONTROLLER_H

#include <dolphinview.h>
#include <kurl.h>
#include <QtCore/QObject>
#include <libdolphin_export.h>

class QAbstractItemView;
class DolphinView;
class KUrl;
class QBrush;
class QPoint;
class QRect;
class QWidget;

// TODO: get rid of all the state duplications in the controller and allow read access
// to the Dolphin view for all view implementations

/**
 * @brief Acts as mediator between the abstract Dolphin view and the view
 *        implementations.
 *
 * The abstract Dolphin view (see DolphinView) represents the parent of the controller.
 * The lifetime of the controller is equal to the lifetime of the Dolphin view.
 * The controller is passed to the current view implementation
 * (see DolphinIconsView, DolphinDetailsView and DolphinColumnView)
 * by passing it in the constructor and informing the controller about the change
 * of the view implementation:
 *
 * \code
 * QAbstractItemView* view = new DolphinIconsView(parent, controller);
 * controller->setItemView(view);
 * \endcode
 *
 * The communication of the view implementations to the abstract view is done by:
 * - triggerContextMenuRequest()
 * - requestActivation()
 * - triggerUrlChangeRequest()
 * - indicateDroppedUrls()
 * - indicateSortingChange()
 * - indicateSortOrderChanged()
 * - setZoomInPossible()
 * - setZoomOutPossible()
 * - triggerItem()
 * - handleKeyPressEvent()
 * - emitItemEntered()
 * - emitViewportEntered()
 *
 * The communication of the abstract view to the view implementations is done by:
 * - setUrl()
 * - setShowHiddenFiles()
 * - setShowPreview()
 * - indicateActivationChange()
 * - triggerZoomIn()
 * - triggerZoomOut()
 */
class LIBDOLPHINPRIVATE_EXPORT DolphinController : public QObject
{
    Q_OBJECT

public:
    explicit DolphinController(DolphinView* dolphinView);
    virtual ~DolphinController();

    /**
     * Allows read access for the the view implementation to the abstract
     * Dolphin view.
     */
    const DolphinView* dolphinView() const;

    /**
     * Sets the URL to \a url and emits the signal urlChanged() if
     * \a url is different for the current URL. This method should
     * be invoked by the abstract Dolphin view whenever the current
     * URL has been changed.
     */
    void setUrl(const KUrl& url);
    const KUrl& url() const;

    /**
     * Changes the current item view where the controller is working. This
     * is only necessary for views like the tree view, where internally
     * several QAbstractItemView instances are used.
     */
    void setItemView(QAbstractItemView* view);

    QAbstractItemView* itemView() const;

    /**
     * Allows a view implementation to request an URL change to \a url.
     * The signal requestUrlChange() is emitted and the abstract Dolphin view
     * will assure that the URL of the Dolphin Controller will be updated
     * later. Invoking this method makes only sense if the view implementation
     * shows a hierarchy of URLs and allows to change the URL within
     * the view (e. g. this is the case in the column view).
     */
    void triggerUrlChangeRequest(const KUrl& url);

    /**
     * Requests a context menu for the position \a pos. This method
     * should be invoked by the view implementation when a context
     * menu should be opened. The abstract Dolphin view itself
     * takes care itself to get the selected items depending from
     * \a pos.
     */
    void triggerContextMenuRequest(const QPoint& pos);

    /**
     * Requests an activation of the view and emits the signal
     * activated(). This method should be invoked by the view implementation
     * if e. g. a mouse click on the view has been done.
     * After the activation has been changed, the view implementation
     * might listen to the activationChanged() signal.
     */
    void requestActivation();

    /**
     * Indicates that URLs are dropped above a destination. This method
     * should be invoked by the view implementation. The abstract Dolphin view
     * will start the corresponding action (copy, move, link).
     * @param urls      URLs that are dropped above a destination.
     * @param destPath  Path of the destination.
     * @param destItem  Destination item (can be null, see KFileItem::isNull()).
     */
    void indicateDroppedUrls(const KUrl::List& urls,
                             const KUrl& destPath,
                             const KFileItem& destItem);

    /**
     * Informs the abstract Dolphin view about a sorting change done inside
     * the view implementation. This method should be invoked by the view
     * implementation (e. g. the details view uses this method in combination
     * with the details header).
     */
    void indicateSortingChange(DolphinView::Sorting sorting);

    /**
     * Informs the abstract Dolphin view about a sort order change done inside
     * the view implementation. This method should be invoked by the view
     * implementation (e. g. the details view uses this method in combination
     * with the details header).
     */
    void indicateSortOrderChange(Qt::SortOrder order);

    /**
     * Informs the abstract Dolphin view about an additional information change
     * done inside the view implementation. This method should be invoked by the
     * view implementation (e. g. the details view uses this method in combination
     * with the details header).
     */
    void indicateAdditionalInfoChange(const KFileItemDelegate::InformationList& info);

    /**
     * Informs the view implementation about a change of the activation
     * state and is invoked by the abstract Dolphin view. The signal
     * activationChanged() is emitted.
     */
    void indicateActivationChange(bool active);

    /**
     * Tells the view implementation to zoom in by emitting the signal zoomIn()
     * and is invoked by the abstract Dolphin view.
     */
    void triggerZoomIn();

    /**
     * Is invoked by the view implementation to indicate whether a zooming in
     * is possible. The abstract Dolphin view updates the corresponding menu
     * action depending on this state.
     */
    void setZoomInPossible(bool possible);
    bool isZoomInPossible() const;

    /**
     * Tells the view implementation to zoom out by emitting the signal zoomOut()
     * and is invoked by the abstract Dolphin view.
     */
    void triggerZoomOut();

    /**
     * Is invoked by the view implementation to indicate whether a zooming out
     * is possible. The abstract Dolphin view updates the corresponding menu
     * action depending on this state.
     */
    void setZoomOutPossible(bool possible);
    bool isZoomOutPossible() const;

    /**
     * Should be invoked in each view implementation whenever a key has been
     * pressed. If the selection model of \a view is not empty and
     * the return key has been pressed, the selected items will get triggered.
     */
    void handleKeyPressEvent(QKeyEvent* event);

    /**
     * Returns the file item for the proxy index \a index of the view \a view.
     */
    KFileItem itemForIndex(const QModelIndex& index) const;

public slots:
    /**
     * Emits the signal itemTriggered() if the file item for the index \a index
     * is not null. The method should be invoked by the
     * controller parent whenever the user has triggered an item.
     */
    void triggerItem(const QModelIndex& index);

    /**
     * Emits the signal itemEntered() if the file item for the index \a index
     * is not null. The method should be invoked by the controller parent
     * whenever the mouse cursor is above an item.
     */
    void emitItemEntered(const QModelIndex& index);

    /**
     * Emits the signal viewportEntered(). The method should be invoked by
     * the controller parent whenever the mouse cursor is above the viewport.
     */
    void emitViewportEntered();

signals:
    /**
     * Is emitted if the URL for the Dolphin controller has been changed
     * to \a url.
     */
    void urlChanged(const KUrl& url);

    /**
     * Is emitted if the view implementation requests a changing of the current
     * URL to \a url (see triggerUrlChangeRequest()).
     */
    void requestUrlChange(const KUrl& url);

    /**
     * Is emitted if a context menu should be opened (see triggerContextMenuRequest()).
     * The abstract Dolphin view connects to this signal and will open the context menu.
     * @param pos       Position relative to the view widget where the
     *                  context menu should be opened. It is recommended
     *                  to get the corresponding model index from
     *                  this position.
     */
    void requestContextMenu(const QPoint& pos);

    /**
     * Is emitted if the view has been activated by e. g. a mouse click.
     * The abstract Dolphin view connects to this signal to know the
     * destination view for the menu actions.
     */
    void activated();

    /**
     * Is emitted if the URLs \a urls have been dropped to the destination
     * path \a destPath. If the URLs have been dropped above an item of
     * the destination path, the item is indicated by \a destItem
     * (can be null, see KFileItem::isNull()).
     */
    void urlsDropped(const KUrl::List& urls,
                     const KUrl& destPath,
                     const KFileItem& destItem);

    /**
     * Is emitted if the sorting has been changed to \a sorting by
     * the view implementation (see indicateSortingChanged().
     * The abstract Dolphin view connects to
     * this signal to update its menu action.
     */
    void sortingChanged(DolphinView::Sorting sorting);

    /**
     * Is emitted if the sort order has been changed to \a order
     * by the view implementation (see indicateSortOrderChanged().
     * The abstract Dolphin view connects
     * to this signal to update its menu actions.
     */
    void sortOrderChanged(Qt::SortOrder order);

    /**
     * Is emitted if the additional info has been changed to \a info
     * by the view implementation. The abstract Dolphin view connects
     * to this signal to update its menu actions.
     */
    void additionalInfoChanged(const KFileItemDelegate::InformationList& info);

    /**
     * Is emitted if the activation state has been changed to \a active
     * by the abstract Dolphin view.
     * The view implementation might connect to this signal if custom
     * updates are required in this case.
     */
    void activationChanged(bool active);

    /**
     * Is emitted if the item \a item should be triggered. The abstract
     * Dolphin view connects to this signal. If the item represents a directory,
     * the directory is opened. On a file the corresponding application is opened.
     * The item is null (see KFileItem::isNull()), when clicking on the viewport itself.
     */
    void itemTriggered(const KFileItem& item);

    /**
     * Is emitted if the mouse cursor has entered the item
     * given by \a index (see emitItemEntered()).
     * The abstract Dolphin view connects to this signal.
     */
    void itemEntered(const KFileItem& item);

    /**
     * Is emitted if the mouse cursor has entered
     * the viewport (see emitViewportEntered().
     * The abstract Dolphin view connects to this signal.
     */
    void viewportEntered();

    /**
     * Is emitted if the view should zoom in. The view implementation
     * must connect to this signal if it supports zooming.
     */
    void zoomIn();

    /**
     * Is emitted if the view should zoom out. The view implementation
     * must connect to this signal if it supports zooming.
     */
    void zoomOut();

private:
    bool m_zoomInPossible;
    bool m_zoomOutPossible;
    KUrl m_url;
    DolphinView* m_dolphinView;
    QAbstractItemView* m_itemView;
};

inline const DolphinView* DolphinController::dolphinView() const
{
    return m_dolphinView;
}

inline const KUrl& DolphinController::url() const
{
    return m_url;
}

inline QAbstractItemView* DolphinController::itemView() const
{
    return m_itemView;
}

inline void DolphinController::setZoomInPossible(bool possible)
{
    m_zoomInPossible = possible;
}

inline bool DolphinController::isZoomInPossible() const
{
    return m_zoomInPossible;
}

inline void DolphinController::setZoomOutPossible(bool possible)
{
    m_zoomOutPossible = possible;
}

inline bool DolphinController::isZoomOutPossible() const
{
    return m_zoomOutPossible;
}

#endif
