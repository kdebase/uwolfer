#ifndef PLUGIN_PATHS_H
#define PLUGIN_PATHS_H

#include <QStringList>

extern QStringList getSearchPaths();

#endif
