#include "shutdowndlg.h"
#include <kcmdlineargs.h>
#include <kaboutdata.h>
#include <kapplication.h>
#include <kiconloader.h>
#include <kworkspace.h>

int
main(int argc, char *argv[])
{
   KAboutData about("kapptest", 0, ki18n("kapptest"), "version");
   KCmdLineArgs::init(argc, argv, &about);

   KApplication a;
   KIconLoader::global()->addAppDir("ksmserver");
   KSMShutdownFeedback::start();

   KWorkSpace::ShutdownType sdtype = KWorkSpace::ShutdownTypeNone;
   QString bopt;
   (void)KSMShutdownDlg::confirmShutdown( true, sdtype, bopt );
/*   (void)KSMShutdownDlg::confirmShutdown( false, sdtype, bopt ); */

   KSMShutdownFeedback::stop();
}
