/*
  Copyright (C) 1999 Daniel M. Duley <mosfet@kde.org>

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public
  License as published by the Free Software Foundation; either
  version 2 of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; see the file COPYING.  If not, write to
  the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
  Boston, MA 02110-1301, USA.
 */

#ifndef __BTNHIGHCOLOR_H
#define __BTNHIGHCOLOR_H

/* XPM */
static const char * btnhighcolor_xpm[] = {
"14 15 75 1",
" 	c None",
".	c #6E6E6E",
"+	c #757575",
"@	c #878787",
"#	c #7D7D7D",
"$	c #9E9E9E",
"%	c #B9B9B9",
"&	c #C6C6C6",
"*	c #BABABA",
"=	c #A5A5A5",
"-	c #7F7F7F",
";	c #848484",
">	c #A7A7A7",
",	c #BFBFBF",
"'	c #D1D1D1",
")	c #D7D7D7",
"!	c #DADADA",
"~	c #CBCBCB",
"{	c #ABABAB",
"]	c #B3B3B3",
"^	c #C2C2C2",
"/	c #CACACA",
"(	c #C9C9C9",
"_	c #B6B6B6",
":	c #9A9A9A",
"<	c #999999",
"[	c #B0B0B0",
"}	c #C4C4C4",
"|	c #C3C3C3",
"1	c #C0C0C0",
"2	c #AEAEAE",
"3	c #969696",
"4	c #C1C1C1",
"5	c #CCCCCC",
"6	c #C5C5C5",
"7	c #BEBEBE",
"8	c #AAAAAA",
"9	c #CECECE",
"0	c #D4D4D4",
"a	c #DBDBDB",
"b	c #DEDEDE",
"c	c #D5D5D5",
"d	c #D3D3D3",
"e	c #BCBCBC",
"f	c #CDCDCD",
"g	c #E0E0E0",
"h	c #E4E4E4",
"i	c #E8E8E8",
"j	c #EBEBEB",
"k	c #E9E9E9",
"l	c #E6E6E6",
"m	c #DDDDDD",
"n	c #E1E1E1",
"o	c #EDEDED",
"p	c #F1F1F1",
"q	c #F5F5F5",
"r	c #F8F8F8",
"s	c #F6F6F6",
"t	c #F3F3F3",
"u	c #EEEEEE",
"v	c #E5E5E5",
"w	c #DCDCDC",
"x	c #B7B7B7",
"y	c #E2E2E2",
"z	c #FDFDFD",
"A	c #FFFFFF",
"B	c #FCFCFC",
"C	c #F7F7F7",
"D	c #B5B5B5",
"E	c #F2F2F2",
"F	c #FAFAFA",
"G	c #9B9B9B",
"H	c #FBFBFB",
"I	c #A9A9A9",
"J	c #747474",
"     ....     ",
"   ..+@@+..   ",
"  .#$%&&*=-.  ",
" .;>,')!)~{#. ",
" .$]^///(&_:. ",
".<[*^}||11*23.",
".[4&5555~(678.",
".,90!aba)cd~e.",
".faghijklhm06.",
".'nopqrstuvw/.",
".xyprzAzBCunD.",
" .'EzAAAAFpf. ",
" .GcHAAAAF0<. ",
"  ..I5kk5I..  ",
"    J.....    "};

#endif // __BTNHIGHCOLOR_H
