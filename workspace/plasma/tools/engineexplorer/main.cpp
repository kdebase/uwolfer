/*
 *   Copyright 2006 Aaron Seigo <aseigo@kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as
 *   published by the Free Software Foundation; either version 2,
 *   or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <KApplication>
#include <KAboutData>
#include <KCmdLineArgs>
#include <KLocale>

#include "engineexplorer.h"

static const char description[] = I18N_NOOP("Explore the data published by Plasma DataEngines");
static const char version[] = "0.0";

int main(int argc, char **argv)
{
    KAboutData aboutData("plasmaengineexplorer", 0, ki18n("Plasma Engine Explorer"),
                         version, ki18n(description), KAboutData::License_GPL,
                         ki18n("(c) 2006, The KDE Team"));
    aboutData.addAuthor(ki18n("Aaron J. Seigo"),
                        ki18n( "Author and maintainer" ),
                        "aseigo@kde.org");


    KCmdLineArgs::init(argc, argv, &aboutData);

    KApplication app;
    EngineExplorer* w = new EngineExplorer;
    w->show();
    return app.exec();
}
